# python-boilerplate

some boilerplate code for any python project

## Things to Do

1. create virtual env
2. `pip install -r requirements.txt`
3. format code with `./format.sh`
4. if using test-commit-rever , `./tcr.sh some commit message`

